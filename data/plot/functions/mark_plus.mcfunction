function plot:get_corner
summon area_effect_cloud ~ ~ ~ {Tags:["mark"]}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
execute at @e[tag=mark,limit=1] unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]

scoreboard players operation #corner plot.x -= #combsize plot.var
summon area_effect_cloud ~ ~ ~ {Tags:["mark"]}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
execute at @e[tag=mark,limit=1] unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]

scoreboard players operation #corner plot.x += #combsize plot.var
scoreboard players operation #corner plot.x += #combsize plot.var
summon area_effect_cloud ~ ~ ~ {Tags:["mark"]}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
execute at @e[tag=mark,limit=1] unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]

scoreboard players operation #corner plot.x -= #combsize plot.var
scoreboard players operation #corner plot.z -= #combsize plot.var
summon area_effect_cloud ~ ~ ~ {Tags:["mark"]}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
execute at @e[tag=mark,limit=1] unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]

scoreboard players operation #corner plot.z += #combsize plot.var
scoreboard players operation #corner plot.z += #combsize plot.var
summon area_effect_cloud ~ ~ ~ {Tags:["mark"]}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
execute at @e[tag=mark,limit=1] unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]
