tag @e[tag=plot_marker] remove matched
execute as @e[tag=plot_marker] if score @s plot.id = @p plot.id run tag @s add matched
tag @a remove owner
execute as @a if score @s plot.owner = @e[tag=matched,limit=1] plot.owner run tag @s add owner
execute unless entity @s[tag=owner] run tellraw @s {"text":"This isn't your plot.","color":"red"}
execute if entity @s[tag=owner] run tellraw @s {"text":"Unclaimed.","color":"gold"}
execute if entity @s[tag=owner] run scoreboard players reset @e[tag=matched] plot.owner
