execute as @e[tag=warp] unless entity @s[nbt={Age:0}] at @s run fill ~ ~-1 ~ ~ ~-1 ~ air replace end_gateway
#kill @e[tag=warp]
#execute as @a unless score @s plot.leaveGame matches 0.. store result score #height plot.var run data get entity @s Pos[1]
execute as @a unless score @s plot.leaveGame matches 0 run function plot:on_join
execute as @a unless score @s plot.leaveGame matches 0 run scoreboard players set @s plot.leaveGame 0
execute if score #ready plot.var matches 4 run function plot:actions/tick
execute if score #ready plot.var matches 0 run function plot:setup/tick
execute if score #ready plot.var matches 1 positioned 5713000 0 5713000 run function plot:check_unloaded
#execute if score #ready plot.var matches 1 if score #unloaded plot.var matches 0 run say loaded
execute if score #ready plot.var matches 1 if score #unloaded plot.var matches 0 as @a at @s run function plot:setup/run2
execute if score #ready plot.var matches 2 as @p at @s unless block ~ ~ ~ air run function plot:setup/run3
execute if score #ready plot.var matches 3 as @p at @s run function plot:setup/run4
scoreboard players add @a[scores={plot.tick=0..}] plot.tick 1
scoreboard players add #global plot.tick 1
execute if score #global plot.tick matches 20 as @a at @s run function plot:second
execute if score #global plot.tick matches 20.. run scoreboard players set #global plot.tick 0
