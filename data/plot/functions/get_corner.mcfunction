execute store result score @s plot.x run data get entity @s Pos[0]
execute store result score @s plot.z run data get entity @s Pos[2]
scoreboard players operation #corner plot.x = @s plot.x
scoreboard players operation #corner plot.z = @s plot.z
scoreboard players operation #corner plot.x /= #combsize plot.var
scoreboard players operation #corner plot.z /= #combsize plot.var
scoreboard players operation #corner plot.x *= #combsize plot.var
scoreboard players operation #corner plot.z *= #combsize plot.var
