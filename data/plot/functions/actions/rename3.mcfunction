#say rename3
tag @e[tag=plot_marker] remove matched
execute as @e[tag=plot_marker] if score @s plot.id = @p plot.id run tag @s add matched
tag @a remove owner
execute as @a if score @s plot.owner = @e[tag=matched,limit=1] plot.owner run tag @s add owner
execute if entity @e[tag=matched,name="Armor Stand",nbt={Marker:1b}] run tellraw @s {"text":"Rename the armor stand.","color":"aqua"}
# TODO: Minecraft 1.14 will allow copying the name
execute if entity @e[tag=matched,name="Armor Stand",nbt={Marker:1b}] at @e[tag=matched] run tp @s ~ ~ ~
execute if entity @e[tag=matched,name="Armor Stand",nbt={Marker:1b}] run data merge entity @e[tag=matched,limit=1] {Marker:0b,Invisible:0b}
execute if entity @e[tag=matched,name=!"Armor Stand"] run tellraw @s {"text":"Renamed.","color":"gold"}
execute if entity @e[tag=matched,name=!"Armor Stand"] run data merge entity @e[tag=matched,limit=1] {Marker:1b,Invisible:1b}
execute if entity @e[tag=matched,name=!"Armor Stand"] run clear @s name_tag{plotTag:1}
