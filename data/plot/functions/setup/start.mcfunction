scoreboard players set @s plot.size 0
scoreboard players set @s plot.roadsize 0
scoreboard players set @s plot.height 0
scoreboard players set @s plot.grid 0
scoreboard players set @s plot.action 0
scoreboard players enable @s plot.size
scoreboard players enable @s plot.roadsize
scoreboard players enable @s plot.height
scoreboard players enable @s plot.grid
scoreboard players enable @s plot.action
scoreboard players operation #combsize plot.var = #size plot.var
scoreboard players operation #combsize plot.var += #roadsize plot.var
tellraw @s [{"text":"\nSETUP\n=====","color":"red"}]
tellraw @s [{"text":"Plot Size: ","color":"gold"},{"score":{"name":"#size","objective":"plot.var"},"color":"white"},{"text":"    "},{"text":"[-1] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.size set -1"}},{"text":"[+1]","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.size set 1"}},{"text":" [-10] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.size set -10"}},{"text":"[+10]","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.size set 10"}}]
tellraw @s [{"text":"Road Size: ","color":"gold"},{"score":{"name":"#roadsize","objective":"plot.var"},"color":"white"},{"text":"    "},{"text":"[-1] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.roadsize set -1"}},{"text":"[+1]","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.roadsize set 1"}}]
tellraw @s [{"text":"Ground Level: ","color":"gold"},{"score":{"name":"#height","objective":"plot.var"},"color":"white"},{"text":"    "},{"text":"[Set] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.height set 1"}}]
#tellraw @s [{"text":"Grid Size: ","color":"gold"},{"score":{"name":"#grid","objective":"plot.var"},"color":"white"},{"text":"    "},{"text":"[-1] ","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.grid set -1"}},{"text":"[+1]","color":"green","clickEvent":{"action":"run_command","value":"/trigger plot.grid set 1"}}]
tellraw @s [{"text":"[START] ","color":"red","clickEvent":{"action":"run_command","value":"/trigger plot.action set 1"}}]
