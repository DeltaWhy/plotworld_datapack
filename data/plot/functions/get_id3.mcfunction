tag @e remove matched
say found
tag @e[tag=plot_marker,sort=nearest,limit=1] add matched
execute if entity @e[tag=matched,name="Armor Stand"] unless score @e[tag=matched,limit=1] plot.owner matches 1.. run title @s actionbar [{"score":{"name":"@e[tag=matched]","objective":"plot.id"}}," (unclaimed)"]
execute unless entity @e[tag=matched,name="Armor Stand"] unless score @e[tag=matched,limit=1] plot.owner matches 1.. run title @s actionbar [{"selector":"@e[tag=matched]"}," (unclaimed)"]
tag @a remove owner
execute as @a if score @s plot.owner = @e[tag=matched,limit=1] plot.owner run tag @s add owner
execute if entity @e[tag=matched,name="Armor Stand"] if entity @p[tag=owner] run title @s actionbar [{"score":{"name":"@e[tag=matched]","objective":"plot.id"}}," (",{"selector":"@p[tag=owner]"},")"]
execute if entity @e[tag=matched,name="Armor Stand"] if score @e[tag=matched,limit=1] plot.owner matches 1.. unless entity @p[tag=owner] run title @s actionbar [{"score":{"name":"@e[tag=matched]","objective":"plot.id"}}," (claimed)"]
execute unless entity @e[tag=matched,name="Armor Stand"] if entity @p[tag=owner] run title @s actionbar [{"selector":"@e[tag=matched]"}," (",{"selector":"@p[tag=owner]"},")"]
execute unless entity @e[tag=matched,name="Armor Stand"] if score @e[tag=matched,limit=1] plot.owner matches 1.. unless entity @p[tag=owner] run title @s actionbar [{"selector":"@e[tag=matched]"}," (claimed)"]
scoreboard players operation @s plot.id = @e[tag=matched,limit=1] plot.id
