summon area_effect_cloud ~ ~ ~ {Tags:["mark"],Duration:0}
execute store result score @e[tag=mark] plot.x run data get entity @e[tag=mark,limit=1] Pos[0]
execute store result score @e[tag=mark] plot.z run data get entity @e[tag=mark,limit=1] Pos[2]
tellraw @a [{"score":{"name":"@e[tag=mark]","objective":"plot.x"}},{"text":","},{"score":{"name":"@e[tag=mark]","objective":"plot.z"}}]
kill @e[tag=mark]
