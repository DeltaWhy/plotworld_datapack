execute as @e[tag=plot_marker,sort=furthest] unless score @s plot.owner matches 1.. at @s store result score #unclaimed plot.x run data get entity @s Pos[0]
execute as @e[tag=plot_marker,sort=furthest] unless score @s plot.owner matches 1.. at @s store result score #unclaimed plot.z run data get entity @s Pos[2]
