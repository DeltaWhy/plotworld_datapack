execute unless block ~ ~ ~ air run tellraw @s {"text":"Can't warp from here.","color":"red"}
scoreboard players set @s plot.id -1
scoreboard players set @s plot.tick 0
fill ~ ~ ~ ~ ~ ~ end_gateway replace air
execute store result block ~ ~ ~ ExitPortal.X int 1 run scoreboard players get @s plot.homeX
execute store result block ~ ~ ~ ExitPortal.Y int 1 run scoreboard players get @s plot.homeY
execute store result block ~ ~ ~ ExitPortal.Z int 1 run scoreboard players get @s plot.homeZ
data merge block ~ ~ ~ {ExactTeleport:0}
summon area_effect_cloud ~ ~1 ~ {Tags:["warp"],NoGravity:1b,Duration:2147483647}
