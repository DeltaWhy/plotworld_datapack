execute unless score @s plot.owner matches 1.. run scoreboard players add #current plot.owner 1
execute unless score @s plot.owner matches 1.. run scoreboard players operation @s plot.owner = #current plot.owner
tellraw @s [{"text":"Plot World v0.1","color":"aqua"}]
execute unless score #ready plot.var matches 1.. run function plot:setup/start
execute if score #ready plot.var matches 3.. run function plot:actions/start
