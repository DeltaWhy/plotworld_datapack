tag @e[tag=plot_marker] remove matched
execute as @e[tag=plot_marker] if score @s plot.id = @p plot.id run tag @s add matched
tag @a remove owner
execute as @a if score @s plot.owner = @e[tag=matched,limit=1] plot.owner run tag @s add owner
execute if score @e[tag=matched,limit=1] plot.owner matches 1.. unless entity @p[tag=owner] run tellraw @s {"text":"This plot is already claimed.","color":"red"}
execute if entity @s[tag=owner] run tellraw @s {"text":"You already claimed this plot.","color":"gold"}
execute unless entity @s[tag=owner] if entity @p[tag=owner] run tellraw @s [{"text":"This plot is claimed by ","color":"red"},{"selector":"@p[tag=owner]"},"."]
execute unless score @e[tag=matched,limit=1] plot.owner matches 1.. run tellraw @s {"text":"Claimed.","color":"gold"}
execute unless score @e[tag=matched,limit=1] plot.owner matches 1.. run scoreboard players operation @e[tag=matched] plot.owner = @s plot.owner
