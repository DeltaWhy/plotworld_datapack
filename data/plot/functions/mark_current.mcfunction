function plot:get_corner
#tellraw @a [{"score":{"name":"#corner","objective":"plot.x"}},{"text":","},{"score":{"name":"#corner","objective":"plot.z"}}]
summon area_effect_cloud ~ ~ ~ {Tags:["mark"],Duration:3}
execute store result entity @e[tag=mark,limit=1] Pos[0] double 1 run scoreboard players get #corner plot.x
execute store result entity @e[tag=mark,limit=1] Pos[1] double 1 run scoreboard players get #height plot.var
execute store result entity @e[tag=mark,limit=1] Pos[2] double 1 run scoreboard players get #corner plot.z
execute as @e[tag=mark,limit=1] at @s run tp ~0.5 ~ ~0.5
#execute as @e[tag=mark,limit=1] at @s unless entity @e[tag=plot_marker,distance=..1] run say mark
execute as @e[tag=mark,limit=1] at @s unless entity @e[tag=plot_marker,distance=..1] run function plot:mark
kill @e[tag=mark,limit=1]
