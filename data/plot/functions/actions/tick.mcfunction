execute as @a[scores={plot.tick=10..}] at @s run say get_id
execute as @a[scores={plot.tick=10..}] at @s run function plot:say_coords
#execute as @a[scores={plot.tick=10..}] at @s run function plot:get_id
#execute as @a[scores={plot.tick=10..}] at @s run function plot:get_id_3
scoreboard players reset @a[scores={plot.tick=10..}] plot.id
scoreboard players reset @a[scores={plot.tick=10..}] plot.tick
execute as @a at @s if score @s plot.action matches 1 run function plot:actions/claim
execute as @a at @s if score @s plot.action matches 2 run function plot:actions/unclaim
execute as @a at @s if score @s plot.action matches 3 run function plot:actions/rename
execute as @a at @s if score @s plot.action matches 4 run function plot:actions/go_home
execute as @a at @s if score @s plot.action matches 5 run function plot:actions/set_home
execute as @a at @s if score @s plot.action matches 6 run function plot:actions/go_unclaimed
execute as @a store result score #tagCnt plot.var run clear @s name_tag{plotTag:1} 0
execute as @a store result score #tagUnnamed plot.var run clear @s name_tag{plotTag:1,display:{Name:"{\"text\":\"Plot Name\",\"italic\":false}"}} 0
execute as @a at @s if score #tagCnt plot.var matches 1.. unless score #tagUnnamed plot.var matches 1.. run function plot:actions/rename3

execute as @a at @s unless score @s plot.action matches 0 run scoreboard players reset @s plot.id
execute as @a at @s unless score @s plot.id matches -1 unless score @s plot.action matches 0 run function plot:get_id
execute as @a at @s unless score @s plot.action matches 0 run function plot:actions/start
#scoreboard players set @a plot.action 0
