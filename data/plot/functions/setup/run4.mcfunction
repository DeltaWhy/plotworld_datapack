execute positioned -1 0 -1 run data merge entity @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] {CustomName:"{\"text\":\"Spawn\",\"color\":\"green\"}"}
execute positioned -1 0 -1 at @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] run tp @s ~ ~ 4 180 0
execute positioned -1 0 -1 at @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] run setblock ~ ~ ~ sign
execute positioned -1 0 -1 at @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] run data merge block ~ ~ ~ {Text2:"{\"text\":\"Spawn\",\"color\":\"green\"}"}
execute positioned -1 0 -1 at @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] run function plot:get_id3
function plot:actions/start
execute positioned -1 0 -1 at @e[tag=plot_marker,limit=1,dy=255,dx=1,dz=1] run function plot:actions/claim
scoreboard players set #ready plot.var 4
