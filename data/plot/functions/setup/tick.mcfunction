scoreboard players operation #size plot.var += @p plot.size
execute if score #size plot.var matches ..-1 run scoreboard players operation #size plot.var -= @p plot.size
scoreboard players operation #roadsize plot.var += @p plot.roadsize
execute if score #roadsize plot.var matches ..-1 run scoreboard players operation #roadsize plot.var -= @p plot.size
scoreboard players operation #grid plot.var += @p plot.grid
execute as @a if score @s plot.action matches 1 run function plot:setup/run
scoreboard players set @a plot.action 0
execute if score @p plot.height matches 1.. store result score #height plot.var run data get entity @p Pos[1]

execute if score @p plot.size matches ..-1 as @p run function plot:setup/start
execute if score @p plot.size matches 1.. as @p run function plot:setup/start
execute if score @p plot.roadsize matches ..-1 as @p run function plot:setup/start
execute if score @p plot.roadsize matches 1.. as @p run function plot:setup/start
execute if score @p plot.height matches 1.. as @p run function plot:setup/start
execute if score @p plot.grid matches ..-1 as @p run function plot:setup/start
execute if score @p plot.grid matches 1.. as @p run function plot:setup/start

execute if score #ready plot.var matches 1 as @p at @p run function plot:setup/generate
