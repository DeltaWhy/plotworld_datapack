say Plot World Datapack loaded.
scoreboard objectives add plot.id dummy
scoreboard objectives add plot.x dummy
scoreboard objectives add plot.z dummy
scoreboard objectives add plot.homeX dummy
scoreboard objectives add plot.homeY dummy
scoreboard objectives add plot.homeZ dummy
scoreboard objectives add plot.var dummy
scoreboard objectives add plot.owner dummy
scoreboard objectives add plot.tick dummy
scoreboard players set #global plot.tick 0
scoreboard objectives add plot.help trigger
scoreboard objectives add plot.size trigger
scoreboard objectives add plot.roadsize trigger
scoreboard objectives add plot.height trigger
scoreboard objectives add plot.grid trigger
scoreboard objectives add plot.action trigger
scoreboard objectives add plot.leaveGame minecraft.custom:minecraft.leave_game
scoreboard objectives add plot.const dummy
scoreboard players set #-1 plot.const -1
scoreboard players set #0 plot.const 0
scoreboard players set #1 plot.const 1
scoreboard players set #2 plot.const 2
scoreboard players set #3 plot.const 3
scoreboard players set #4 plot.const 4
execute unless score #height plot.var matches 0.. run scoreboard players set #height plot.var 4
execute unless score #size plot.var matches 0.. run scoreboard players set #size plot.var 32
execute unless score #roadsize plot.var matches 0.. run scoreboard players set #roadsize plot.var 3
execute unless score #grid plot.var matches 0.. run scoreboard players set #grid plot.var 10
scoreboard players add #ready plot.var 0
execute unless score #current plot.id matches 1.. run scoreboard players set #current plot.id 1
execute unless score #current plot.owner matches 0.. run scoreboard players set #current plot.owner 0

gamerule doDaylightCycle false
gamerule doFireTick false
gamerule doWeatherCycle false
gamerule keepInventory true
gamerule mobGriefing false

execute as @p run function plot:on_join
